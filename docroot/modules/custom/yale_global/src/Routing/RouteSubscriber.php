<?php

namespace Drupal\yale_global\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

/**
 * Class RouteSubscriber.
 */
class RouteSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection) {
    // Provide our own accessible search controller.
    if ($search_route = $collection->get('search.view_node_search')) {
      $search_route->setDefault(
        '_controller',
        'Drupal\yale_global\Controller\YaleSearchController::view'
      );
    };

  }

}
