# Yale Global
Common configuration and functionality for Yale sites.

## Features Components

### Roles

 - __Site Administrator__
   User management
 - __Site Builder__
   Can create content types and views; create blocks; build landing pages;
 - __Editor__
   Ability to create and approve content

Features currently removes permissions from role exports to avoid a potential
problem with multiple features defining the same role (https://www.drupal.org/node/2383439).

Since the roles defined in this module are unique to `yale_global` and not
controlled by any other modules, the permissions are safe to include in the
role configuration without risk of conflict. A patch is applied to the features
module to ensure that the permissions continue to be exportable, rather than
requiring manually changes to the YML file (https://www.drupal.org/node/2726409).

