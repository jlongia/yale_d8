var gulp = require('gulp');
var $    = require('gulp-load-plugins')();
var replace = require('gulp-replace');

var sassPaths = [
  'bower_components/foundation-sites/scss',
  'bower_components/motion-ui/src'
];

gulp.task('sass', function() {
  return gulp.src('scss/yale_foundation.scss')
    .pipe($.sass({
        includePaths: sassPaths
      })
      .on('error', $.sass.logError))
    .pipe($.autoprefixer({
      browsers: ['last 2 versions', 'ie >= 9']
    }))
    .pipe(gulp.dest('css'));
});

gulp.task('sass-dev', function() {
  return gulp.src('scss/yale_foundation.scss')
    .pipe($.sourcemaps.init())
    .pipe($.sass({
        includePaths: sassPaths
      })
      .on('error', $.sass.logError))
    .pipe($.autoprefixer({
      browsers: ['last 2 versions', 'ie >= 9']
    }))
    .pipe($.sourcemaps.write())
    .pipe(gulp.dest('css'));
});

gulp.task('copy', function() {
  gulp.src('node_modules/yaleui/**/*')
    .pipe(gulp.dest('libraries/yaleui'));
});

gulp.task('build-dev', ['sass-dev', 'copy']);

gulp.task('build', ['sass', 'copy']);

gulp.task('watch', ['sass', 'copy'], function () {
  gulp.watch(['scss/**/*.scss'], ['sass']);
});

gulp.task('default', ['build']);
